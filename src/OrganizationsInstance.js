import React, { Component } from 'react';
import Breadcrumbs from './Breadcrumbs';
import SideWidget from './SideWidget';
import axios from 'axios';
import { Timeline } from 'react-twitter-widgets';

class OrganizationsInstance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            Name: '',
            Mission: '',
            AddressLine: '',
            CityStateZipCountry: '',
            ContactName: '',
            ContactEmail: '',
            Link: '',
            Image: '',
            SocialMediaUrls: '',
            ProfileLevel: '',
            NTEECode: '',
            AreasServed: '',
            Twitter: '',
            OutbreakIds: [],
            outbreaks: [],
            countries: [],
        }
    }
    parents = ['Home', 'Organizations'];

    getAsString(str) {
        str = str.replace('[', '');
        str = str.replace(']', '');
        str = str.replace(/'/gi, '');
        return str;
    }

    componentDidMount() {
        const url = 'http://api.epidemicrelief.me/organizations?org_id=' + this.state.id;
        axios.get(url).then(response => {
            console.log(response.data);
            response.data.forEach(org => {
                this.setState({Name: org.name});
                this.setState({Mission: org.mission});
                this.setState({AddressLine: org.address});
                this.setState({CityStateZipCountry: org.citystatezipcountry});
                this.setState({ContactName: org["contact name"]});
                this.setState({ContactEmail: org["contact email"]});
                this.setState({Link: org.link});
                this.setState({Image: org.image});
                this.setState({SocialMediaUrls: org["social media URL"]});
                this.setState({ProfileLevel: org["profile level"]});
                this.setState({NTEECode: this.getAsString(org.ntee)});
                this.setState({AreasServed: this.getAsString(org["areas served"])});
                this.setState({Twitter: org.twitter});
                this.setState({OutbreakIds: this.getAsString(org["outbreak_ids"]).split(", ")});
            });
            // Parse social media urls for Twitter url if exists
            let regex = /https:\/\/twitter.com\/.*?'/;  // Need '?' after '*' to make it non-greedy
            let twitterArr = regex.exec(this.state.SocialMediaUrls);
            if (!!twitterArr) {
                let twitterUrl = twitterArr[0];
                this.setState({Twitter: twitterUrl.substr(0, twitterArr[0].length - 1)});
            }
            // Get related Outbreaks
            this.getOutbreaks();
            this.getCountries();
        });
    }

    getOutbreaks() {
        const url = 'http://api.epidemicrelief.me/outbreaks?outbreak_id=';
        let newOutbreaks = [];
        for (const id in this.state.OutbreakIds) {
            axios.get(url + id).then(response => {
                response.data.forEach(outbreak => {
                    newOutbreaks.push({
                        id: outbreak.outbreak_id,
                        Name: outbreak.disease + ' in ' + outbreak.country
                    });
                });
                this.setState({outbreaks: newOutbreaks});
            });
        }
    }

    getCountries() {
        const url = 'http://api.epidemicrelief.me/countries';
        this.state.AreasServed.split(", ").forEach(area => {
            axios.get(url + '?region=' + area).then(response => {
                const newCountries = response.data.map(country => {
                    return {
                        id: country.code,
                        Name: country.name
                    };
                });
                this.setState({countries: this.state.countries.concat(newCountries)});
            });
            console.log(this.state.countries);
        });
    }

    render() {
        return (
            <div className="App">
                <h1 className="mt-4 mb-3" align="left">{this.state.Name}
                    <small/>
                </h1>
                <Breadcrumbs parents={this.parents} current={this.state.Name}/>

                <div className="row">
                    <div className="col-lg-8">
                        <img className="img-fluid rounded"
                             src={this.state.Image} alt={this.state.Name}/>

                        <hr/>

                        <p className="lead" align="left">Mission</p>
                        <p align="left">{this.state.Mission}</p>

                        <p className="lead" align="left">Contact</p>
                        <p align="left">{this.state.ContactName}<br/>
                            {this.state.ContactEmail}<br/>
                            <a href={'http://' + this.state.Link} target="_blank">{this.state.Link}</a><br/>
                        </p>
                        <p className="lead" align="left">More Info</p>
                        <p align="left">
                            NTEE Codes: {this.state.NTEECode}<br/>
                            GuideStar Profile Level: {this.state.ProfileLevel}<br/>
                            Areas Served: {this.state.AreasServed}
                        </p>

                        <hr/>

                        <TwitterFeed Twitter={this.state.Twitter} Name={this.state.Name}/>
                    </div>

                    <div className="col-md-4">
                        <SideWidget title="Address" items={[this.state.AddressLine, this.state.CityStateZipCountry]}/>
                        <SideWidget title="Outbreaks" items={this.state.outbreaks}/>
                        <SideWidget title="Countries" items={this.state.countries}/>
                    </div>
                </div>
            </div>
        );
    }
}

const TwitterFeed = props => {
    const TwitterLink = props.Twitter;
    const Name = props.Name;
    if (!TwitterLink) {
        return (
            <div/>
        )
    } else {
        return (
            <div>
                <a className="twitter-timeline" data-height="800" href={TwitterLink + '?ref_src=twsrc%5Etfw'}>
                    Tweets by {Name}</a>
                <script async src="https://platform.twitter.com/widgets.js" charSet="utf-8"/>
                <hr/>
            </div>
        )
    }
};

export default OrganizationsInstance;