import React, { Component } from 'react';
import Carousel from './Carousel';
import CardLink from './CardLink';

class Home extends Component {
  render() {
    return (
        <div>
            <Carousel/>
            <h1 className="my-4">Welcome to Epidemic Relief</h1>
            <h6>
                This website is designed to educate visitors on ongoing epidemics across the globe and connect them to organizations related to the outbreak.
            </h6>
            <hr/>
            <div className="container">
                <div className="row">
                    <CardLink model="outbreaks"/>
                    <CardLink model="organizations"/>
                    <CardLink model="countries"/>
                </div>
            </div>
        </div>
    );
  }
}

export default Home;