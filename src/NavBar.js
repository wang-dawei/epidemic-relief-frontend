import React, {Component} from 'react';
import history from './history';

class NavBar extends Component {
  handleSubmit(event) {
    let term = document.getElementById("searchBox").value;
    if (!!term) {
      history.push("/search/" + term);
    }
  }

  render() {
    return (
      <nav className="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div className="container">
          <a className="navbar-brand" href="/">Epidemic Relief</a>
          <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
              data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
              aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"/>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <a className="nav-link" href="/outbreaks">Outbreaks</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/organizations">Organizations</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/countries">Countries</a>
              </li>
              <li className="nav-item">
                  <a className="nav-link" href="/visualizations">Visualizations</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/about">About</a>
              </li>
              <form className="form-inline justify-content-center">
                  <div className="form-group" style={{margin: '0px', padding: '5px'}}>
                      <input type="text" className="form-control form-control-sm" id="searchBox" placeholder="Search..."
                             style={{backgroundColor: '#343a40', color: '#fff', maxWidth: '200px'}}/>
                  </div>
                  <button className="btn btn-secondary btn-sm" id="searchButton" onClick={this.handleSubmit}>Go</button>
              </form>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
