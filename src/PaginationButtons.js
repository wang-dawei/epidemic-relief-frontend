import React, { Component } from 'react';

class PaginationButtons extends Component {
  render() {
    const { pageNumber, backClick, forwardClick } = this.props;
    return (
      <ul className="pagination justify-content-center">
        <li className="page-item">
          <a className="page-link" href="#" onClick={backClick} aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span className="sr-only">Previous</span>
          </a>
        </li>
        <li className="page-item" style={{padding: '8px'}}>
          {pageNumber}
        </li>
        <li className="page-item">
          <a className="page-link" href="#" onClick={forwardClick} aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span className="sr-only">Next</span>
          </a>
        </li>
      </ul>
    );
  }
}

export default PaginationButtons;