import React, { Component } from 'react';
import Breadcrumbs from './Breadcrumbs';
import SideWidget from './SideWidget';
import axios from 'axios';

class CountriesInstance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Code: props.match.params.id,
            Capital: '',
            Region: '',
            Subregion: '',
            Name: '',
            LifeExpectancy: '',
            Mortality: '',
            Population: '',
            Flag: '',
            Area: '',
            organizations: [],
            outbreaks: [],
        }
    }
    parents = ['Home', 'Countries'];

    componentDidMount() {
        const url = 'http://api.epidemicrelief.me/countries?code=' + this.state.Code;
        console.log(url);
        axios.get(url).then(response => {
            const data = response.data;
            data.forEach(country => {  // Should only have one country
                console.log(country);
                this.setState({Capital: country.capital});
                this.setState({Region: country.region});
                this.setState({Subregion: country.subregion});
                this.setState({Name: country.name});
                this.setState({LifeExpectancy: country["life expectancy"]});
                this.setState({Mortality: country.mortality});
                this.setState({Population: country.population});
                this.setState({Flag: country.flag});
                this.setState({Area: country.area});
            });
            this.getOrganizations();
            this.getOutbreaks();
        });
    }

    getOrganizations() {
        const url = 'http://api.epidemicrelief.me/organizations';
        axios.get(url + '?area=' + this.state.Region).then(response => {
            const newOrganizations = response.data.map(organization => {
                return {
                    id: organization.org_id,
                    Name: organization.name
                };
            });
            this.setState({organizations: newOrganizations});
        });
    }

    getOutbreaks() {
        const url = 'http://api.epidemicrelief.me/outbreaks?country=';
        axios.get(url + this.state.Name).then(response => {
            const newOutbreaks = response.data.map(outbreak => {
                return {
                    id: outbreak.outbreak_id,
                    Name: outbreak.disease + ' in ' + outbreak.country
                };
            });
            this.setState({outbreaks: newOutbreaks});
        });
    }

    render() {
        return (
            <div className="App">
                <h1 className="mt-4 mb-3" align="left">{this.state.Name}
                    <small/>
                </h1>
                <Breadcrumbs parents={this.parents} current={this.state.Name}/>

                <div className="row">
                    <div className="col-lg-8">
                        <img className="img-fluid rounded"
                             src={this.state.Flag} alt={this.state.Name}/>
                        <hr/>

                        <p className="lead" align="left">Country Info</p>
                        <table style={{width: '100%'}}>
                            <tr align="left">
                                <td>Country Code</td>
                                <td>{this.state.Code}</td>
                            </tr>
                            <tr align="left">
                                <td>Capital</td>
                                <td>{this.state.Capital}</td>
                            </tr>
                            <tr align="left">
                                <td>Region</td>
                                <td>{this.state.Region}</td>
                            </tr>
                            <tr align="left">
                                <td>Subregion</td>
                                <td>{this.state.Subregion}</td>
                            </tr>
                        </table>
                        <hr/>

                        <p className="lead" align="left">Statistics</p>
                        <table style={{width: '100%'}}>
                            <tr align="left">
                                <td>Population</td>
                                <td>{this.state.Population}</td>
                            </tr>
                            <tr align="left">
                                <td>Age-standardized NCD mortality rate (per 100 000 population)</td>
                                <td>{this.state.Mortality}</td>
                            </tr>
                            <tr align="left">
                                <td>Life expectancy at birth (years)</td>
                                <td>{this.state.LifeExpectancy}</td>
                            </tr>
                            <tr align="left">
                                <td>Area (km^2)</td>
                                <td>{this.state.Area}</td>
                            </tr>
                        </table>
                        <hr/>
                    </div>

                    <div className="col-md-4">
                        <SideWidget title="Outbreaks" items={this.state.outbreaks}/>
                        <SideWidget title="Organizations" items={this.state.organizations}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default CountriesInstance;