import React, { Component } from 'react';
import Highlighter from "react-highlight-words";

class OrganizationGridItem extends Component {
    getAsString(str) {
        str = str.replace('[', '');
        str = str.replace(']', '');
        str = str.replace(/'/gi, '');
        return str;
    }

    render() {
        const { link, title, mission, imageUrl, locations, NTEECode, profileLevel, website, searchTerm } = this.props;
        const domain = 'http://';
        this.absoluteUrl = domain.concat(website);
        return (
            <div className="col-lg-4 col-sm-6 portfolio-item">
                <div className="card h-100">
                    <Image imageUrl={imageUrl} link={link} title={title}/>
                    <div className="card-body">
                        <h4 className="card-title">
                            <a href={link}>
                                <Highlighter searchWords={searchTerm} textToHighlight={title}/>
                            </a>
                        </h4>
                        <p className="card-text short-description" align="left">
                            <Highlighter searchWords={searchTerm} textToHighlight={'Mission: ' + mission}/>
                        </p>
                        <p className="card-text" align="left">
                            <Highlighter searchWords={searchTerm} textToHighlight={'Areas Served: ' + this.getAsString(locations)}/>
                        </p>
                        <p className="card-text" align="left">
                            <Highlighter searchWords={searchTerm} textToHighlight={'NTEE Codes: ' + this.getAsString(NTEECode)}/>
                        </p>
                        <p className="card-text" align="left">
                            <Highlighter searchWords={searchTerm} textToHighlight={'Profile Level: ' + profileLevel}/>
                        </p>
                        <p className="card-text" align="left">
                            Website: <a href={this.absoluteUrl} target="_blank">
                                        <Highlighter searchWords={searchTerm} textToHighlight={website}/></a>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

const Image = props => {
    if (!props.imageUrl) {
        return (<div/>);
    } else {
        return (<a href={props.link}><img className="card-img-top" src={props.imageUrl} alt={props.title} style={{maxHeight: '200px',
            maxWidth: '200px', width: 'auto', padding: '10px'}}/></a>);
    }
};

export default OrganizationGridItem;
