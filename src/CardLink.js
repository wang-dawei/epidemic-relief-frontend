import React, {Component} from 'react';
import gray_solid_jpg from './images/gray-solid.jpg';

class CardLink extends Component {
    render() {
        const { model } = this.props;
        let name, description;
        switch (model) {
            case 'outbreaks':
                name = 'Outbreaks';
                description = 'Discover current disease outbreaks in the world and learn more about symptoms, treatment, and prevention.';
                break;
            case 'organizations':
                name = 'Organizations';
                description = 'Organizations and foundations dedicated to fighting disease outbreaks.';
                break;
            case 'countries':
                name = 'Countries';
                description = 'Locations of current outbreaks and operating locations of organizations and foundations.';
                break;
            default:
        }
        return (
            <div className="col-lg-4 mb-4">
                <div className="card h-100">
                    <a href={model}>
                        <img className="img-fluid" src={gray_solid_jpg} alt="gray_solid"/>
                        <div className="card-img-overlay d-flex flex-column justify-content-end">
                            <h3 className="card-title text-dark">{name}</h3>
                            <p className="mt-auto text-dark">{description}</p>
                        </div>
                    </a>
                </div>
            </div>
        );
    }
}

export default CardLink;
