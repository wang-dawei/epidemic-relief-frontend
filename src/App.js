import React, { Component } from 'react';
import './App.css';
import { Switch, Route, Router } from "react-router";
import history from './history';

import NavBar from './NavBar';
import Home from './Home';
import Outbreaks from './Outbreaks';
import OutbreaksInstance from './OutbreaksInstance';
import Countries from './Countries';
import CountriesInstance from './CountriesInstance';
import Organizations from './Organizations';
import OrganizationsInstance from './OrganizationsInstance';
import About from './About';
import SearchPage from './SearchPage';
import Visualizations from './Visualizations';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar/>
        <div className="body-padding">
          <Router history={history}>
            <Switch>
              <Route exact path="/" component={Home}/>
              <div className="container">
                <Route exact path="/outbreaks/:id" component={OutbreaksInstance}/>
                <Route exact path="/outbreaks" component={Outbreaks}/>
                <Route exact path="/organizations/:id" component={OrganizationsInstance}/>
                <Route exact path="/organizations" component={Organizations}/>
                <Route exact path="/countries/:id" component={CountriesInstance}/>
                <Route exact path="/countries" component={Countries}/>
                <Route exact path="/about" component={About}/>
                <Route exact path="/search/:term" component={SearchPage}/>
                <Route exact path="/visualizations" component={Visualizations}/>
              </div>
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}

export default App;