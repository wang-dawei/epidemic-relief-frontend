import React, { Component } from 'react'
import './App.css'
import { scaleLinear } from 'd3-scale'
import { select } from 'd3-selection'

class BarChart extends Component {
    constructor(props) {
        super(props);
        this.createBarChart = this.createBarChart.bind(this);
    }
    componentDidMount() {
        this.createBarChart()
    }
    componentDidUpdate() {
        this.createBarChart()
    }
    createBarChart() {
        const node = this.node;
        const dataMax = this.props.data.reduce((max, d) => d.value > max ? d.value: max, this.props.data[0].value);
        const yScale = scaleLinear()
            .domain([0, dataMax * 1.2])
            .range([0, this.props.size[1] - 200]);
        const height = this.props.size[1];
        const barWidth = this.props.size[0] / (this.props.data.length + this.props.paddingRight);
        const fillColor = this.props.fill;
        const strokeColor = this.props.stroke;
        const textColor = this.props.textColor;
        const padding = 10;

        select(node)
            .selectAll('rect')
            .data(this.props.data)
            .enter()
            .append('rect')
            .attr('width', barWidth)
            .attr('height', d => yScale(d.value))
            .attr('transform', function(d, i) {
                return "translate(" + (i * barWidth + padding) + "," + (height - yScale(d.value) - 200) + ")";
            })
            .style('fill', fillColor)
            .style('stroke', strokeColor);

        select(node)
            .selectAll('text')
            .data(this.props.data)
            .enter()
            .append('text')
            .attr('class', 'label')
            .attr('transform', function(d, i) {
                return "translate(" + (i * barWidth + barWidth/2) + ", " + (height - 190) + ") rotate(45)";
            })
            .attr('font-size', '12px')
            .attr('fill', textColor)
            .text(function(d) {return d.label + ' - ' + d.value;});
    }
    render() {
        return <svg ref={node => this.node = node}
                    width={this.props.size[0]} height={this.props.size[1]}>
        </svg>
    }
}
export default BarChart