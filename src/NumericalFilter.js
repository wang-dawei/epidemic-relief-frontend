import React, { Component } from 'react';

class NumericalFilter extends Component {
    render() {
        const {label, lowId, highId, lowPlaceholder, highPlaceholder, onChange} = this.props;
        return (<tr>
            <td style={{padding: '0px, 30px, 0px, 0px'}}>{label}</td>
            <td><input className="form-control" id={lowId} placeholder={lowPlaceholder} type="text" onChange={onChange} style={{backgroundColor: '#fff', color: '#343a40', maxWidth: '200px', display: 'inline-block'}}/></td>
            <td style={{padding: '0px, 20px, 0px, 20px'}}>to</td>
            <td><input className="form-control" id={highId} placeholder={highPlaceholder} type="text" onChange={onChange} style={{backgroundColor: '#fff', color: '#343a40', maxWidth: '200px', display: 'inline-block'}}/></td>
        </tr>);
    }
}

export default NumericalFilter;