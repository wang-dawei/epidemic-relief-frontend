import React, { Component } from 'react';
import axios from 'axios';

import BarChart from './BarChart';
import Breadcrumbs from './Breadcrumbs';
import PieChart from './PieChart';

class Visualizations extends Component {
    parents = ['Home'];
    current = 'Visualizations';

    constructor(props) {
        super(props);
        this.state = {
            orgsPerType: {'Bronze': 0, 'Silver': 0, 'Gold': 0, 'Platinum': 0},
            outbreaksCount: 0,
            outbreaksPerRegion: [],
            outbreaksPerOrg: [],
        }
    }

    componentDidMount() {
        this.getOrgData();
        this.getOutbreaksPerRegion();
        this.getProvidersConflictsPerCountry();
        this.getProvidersConflictsPerRegion();
        this.getProvidersPopulationPerRegion();
    }

    getOrgData() {
        const url = 'http://api.epidemicrelief.me/organizations?page=1&perPage=1000';
        axios.get(url).then(response => {
            const outbreaksPerOrg = response.data.map(org => ({
                label: org.name,
                value: org.outbreak_ids.split(',').length,
            }));
            let orgsPerType = {'Bronze': 0, 'Silver': 0, 'Gold': 0, 'Platinum': 0};
            response.data.forEach(org => {
                orgsPerType[org['profile level']]++;
            });
            this.setState({
                outbreaksPerOrg: outbreaksPerOrg.sort(function (a,b) {return b.value - a.value}),
                orgsPerType: orgsPerType,
            });
        });
    }

    getOutbreaksPerRegion() {
        const outbreakUrl = 'http://api.epidemicrelief.me/outbreaks?page=1&perPage=1000';
        const countriesUrl = 'http://api.epidemicrelief.me/countries?page=1&perPage=1000';
        let countryCounts = {};
        axios.get(outbreakUrl).then(response => {
            response.data.forEach(outbreak => {
                const name = outbreak.country;
                if (!(name in countryCounts)) {
                    countryCounts[name] = 0;
                }
                countryCounts[name]++;
            });
            this.setState({
                outbreaksCount: response.data.length,
            });
            let regionCounts = {};
            const handleCountry = (countryName => (response => {
                response.data.forEach(country => {
                    const region = country.region;
                    if (!(region in regionCounts)) {
                        regionCounts[region] = 0;
                    }
                    regionCounts[region] += countryCounts[countryName];
                });
                let regionArray = [];
                for (var regionName in regionCounts) {
                    regionArray.push({
                        label: regionName,
                        value: regionCounts[regionName],
                    });
                }
                this.setState({
                    outbreaksPerRegion: regionArray.sort(function(a, b) {return b.value - a.value}),
                });
            }));
            for (var countryName in countryCounts) {
                axios.get(countriesUrl + '&name=' + countryName).then(handleCountry(countryName));
            }

        });
    }

    getProvidersConflictsPerCountry() {
        const url = 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/countries';
        console.log(url);
        axios.get(url).then(response => {
            const conflictsPerCountry = response.data.map(country => ({
                label: country.name,
                value: country.num_ongoing_conflicts,
            }));
            this.setState({
                conflictsPerCountry: conflictsPerCountry.sort(function (a,b) {return b.value - a.value}).slice(0,7),
            });
        });
    }

    getProvidersConflictsPerRegion() {
        const url = 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/countries';
        console.log(url);
        let conflictsPerRegionCounts = {};
        axios.get(url).then(response => {
            response.data.forEach(country => {
                const region = country.region;
                if (!(region in conflictsPerRegionCounts)) {
                    conflictsPerRegionCounts[region] = 0;
                }
                conflictsPerRegionCounts[region] += country.num_ongoing_conflicts;
            });
            this.setState({
                conflictsPerRegion: Object.entries(conflictsPerRegionCounts).sort(function (a,b) {return b[1] - a[1]}),
            });
        });
    }
    getProvidersPopulationPerRegion() {
        const url = 'http://ec2-18-222-112-27.us-east-2.compute.amazonaws.com/countries';
        console.log(url);
        let populationPerRegionCounts = {};
        let populationPerRegion;
        axios.get(url).then(response => {
            response.data.forEach(country => {
                const region = country.region;
                if (!(region in populationPerRegionCounts)) {
                    populationPerRegionCounts[region] = 0;
                }
                populationPerRegionCounts[region] += country.population;
            });
            console.log(populationPerRegionCounts);
            populationPerRegion = Object.entries(populationPerRegionCounts).sort(function (a,b) {return b[1] - a[1]})
                                            .map(item => ({
                                                label: item[0],
                                                value: Math.floor(item[1]/1000000)
                                            }));
            this.setState({
                populationPerRegion: populationPerRegion,
            });
        });
    }

    render() {
        const {outbreaksCount, outbreaksPerRegion, outbreaksPerOrg, orgsPerType, conflictsPerCountry, conflictsPerRegion, populationPerRegion} = this.state;
        const sumOutbreaksPerRegion = outbreaksPerRegion.reduce((function(sum, region) {return sum + region.value}), 0);
        console.log(conflictsPerCountry);
        return (
            <div className='App'>
                <h1 className="mt-4 mb-3" align="left">Visualizations<small/></h1>
                <Breadcrumbs parents={this.parents} current={this.current}/>

                <h4>Our Visualizations</h4>
                <hr/>
                <h6>Number of Outbreaks per Region</h6>
                {outbreaksPerRegion === undefined || outbreaksPerRegion.length === 0 || sumOutbreaksPerRegion !== outbreaksCount ?
                    <p>Loading...</p> :
                    <BarChart data={outbreaksPerRegion}
                          size={[800,500]} fill='#1ABC9C' stroke='#138D75' textColor='#444444' paddingRight={0}/>
                }
                
                <h6>Distribution of Organizations by Profile Level</h6>
                <PieChart data={[
                            {label: 'Bronze', value: orgsPerType['Bronze'], color: '#B9770E'},
                            {label: 'Silver', value: orgsPerType['Silver'], color: '#CACFD2'},
                            {label: 'Gold', value: orgsPerType['Gold'], color: '#F1C40F'},
                            {label: 'Platinum', value: orgsPerType['Platinum'], color: '#808B96'}]}
                          outerRadius={250} size={[500,600]}/>

                <h6>Number of Related Outbreaks per Organization</h6>
                {outbreaksPerOrg === undefined || outbreaksPerOrg.length === 0 ?
                    <p>Loading...</p> :
                    <BarChart data={outbreaksPerOrg}
                          size={[1000,500]} fill='#F39C12' stroke='#D68910' textColor='#444444' paddingRight={3}/>
                }

                <h4>Our Provider's Visualizations</h4>
                <hr/>
                <h6>Number of Ongoing Conflicts per Country (Top 7)</h6>
                {conflictsPerCountry === undefined || conflictsPerCountry.length === 0 ?
                    <p>Loading...</p> :
                    <BarChart data={conflictsPerCountry}
                          size={[800,500]} fill='#C0392B' stroke='#922B21' textColor='#444444' paddingRight={0.1}/>
                }
                
                <h6>Number of Conflicts by Region</h6>
                {conflictsPerRegion === undefined || conflictsPerRegion.length === 0 ?
                    <p>Loading...</p> :
                    <PieChart data={[
                            {label: 'Africa', value: conflictsPerRegion[0][1], color: '#F7DC6F'},
                            {label: 'Europe', value: conflictsPerRegion[3][1], color: '#82E0AA'},
                            {label: 'Asia', value: conflictsPerRegion[1][1], color: '#F1948A'},
                            {label: 'Americas', value: conflictsPerRegion[2][1], color: '#85C1E9'},
                            ]}
                          outerRadius={250} size={[500,600]}/>
                }
                

                <h6>Population by Region (Millions)</h6>
                {populationPerRegion === undefined || populationPerRegion.length === 0 ?
                    <p>Loading...</p> :
                    <BarChart data={populationPerRegion}
                          size={[1000,500]} fill='#AF7AC5' stroke='#884EA0' textColor='#444444' paddingRight={0}/>
                }
            </div>
        );
    }
}

export default Visualizations;