import React, { Component } from 'react';
import axios from 'axios';
import Breadcrumbs from './Breadcrumbs';
import ModelGrid from "./ModelGrid";
import PaginationButtons from './PaginationButtons';

class SearchPage extends Component {
    parents = ['Home'];
    current = 'Search';
    perPage = 3;

    constructor(props) {
        super(props);
        this.state = {
            term: props.match.params.term,
            outbreaks: [],
            outbreaks_page: 1,
            organizations: [],
            organizations_page: 1,
            countries: [],
            countries_page: 1,
        };
    }

    getSearchResults() {
        this.getOutbreaks(this.state.outbreaks_page);
        this.getOrganizations(this.state.organizations_page);
        this.getCountries(this.state.countries_page);
    }

    getOutbreaks(pageNumber) {
        if (pageNumber < 1) {
            return;
        }
        const url = 'http://api.epidemicrelief.me/outbreaks?page=' + pageNumber + '&per_page=' + this.perPage + '&search=' + this.state.term;
        console.log("url: " + url);
        axios.get(url).then(response => {
            const newOutbreaks = response.data.map(outbreak => {
                return {
                    id: outbreak.outbreak_id,
                    Description: outbreak.description,
                    Disease: outbreak.disease,
                    Location: outbreak.country,
                    StartDate: outbreak.date,
                    Transmission: outbreak.transmission,
                    AltNames: outbreak["alternate names"]
                };
            });

            if (newOutbreaks.length > 0) {
                this.setState({
                    outbreaks_page: pageNumber,
                    outbreaks: newOutbreaks
                });
            }
        });
    }

    getOrganizations(pageNumber) {
        if (pageNumber < 1) {
            return;
        }
        const url = 'http://api.epidemicrelief.me/organizations?page=' + pageNumber + '&per_page=' + this.perPage + '&search=' + this.state.term;
        axios.get(url).then(response => {
            const newOrganizations = response.data.map(organization => {
                return {
                    id: organization.org_id,
                    Name: organization.name,
                    Mission: organization.mission,
                    Image: organization.image,
                    AreasServed: organization["areas served"],
                    Link: organization.link,
                    ProfileLevel: organization["profile level"],
                    NTEECode: organization.ntee
                };
            });

            if (newOrganizations.length > 0) {
                this.setState({
                    organizations_page: pageNumber,
                    organizations: newOrganizations
                });
            }
        });
    }

    getCountries(pageNumber) {
        if (pageNumber < 1) {
            return;
        }
        const url = 'http://api.epidemicrelief.me/countries?page=' + pageNumber + '&per_page=' + this.perPage + '&search=' + this.state.term;
        axios.get(url).then(response => {
            const newCountries = response.data.map(country => {
                return {
                    Code: country.code,
                    Capital: country.capital,
                    Region: country.region,
                    Subregion: country.subregion,
                    Name: country.name,
                    LifeExpectancy: country["life expectancy"],
                    Mortality: country.mortality,
                    Population: country.population,
                    Flag: country.flag,
                    Area: country.area
                };
            });

            if (newCountries.length > 0) {
                this.setState({
                    countries_page: pageNumber,
                    countries: newCountries
                });
            }
        })
    }

    onOutbreaksNextClick() {
        this.getOutbreaks(this.state.outbreaks_page + 1);
        console.log('click forward');
    }

    onOutbreaksPrevClick() {
        this.getOutbreaks(this.state.outbreaks_page - 1);
        console.log('click back');
    }

    onCountriesNextClick() {
        this.getCountries(this.state.countries_page + 1);
        console.log('click forward');
    }

    onCountriesPrevClick() {
        this.getCountries(this.state.countries_page - 1);
        console.log('click back');
    }

    onOrganizationsNextClick() {
        this.getOrganizations(this.state.organizations_page + 1);
        console.log('click forward');
    }

    onOrganizationsPrevClick() {
        this.getOrganizations(this.state.organizations_page - 1);
        console.log('click back');
    }

    componentDidMount() {
        this.getSearchResults();
    }

    render() {
        const boundOutbreaksNextClick = this.onOutbreaksNextClick.bind(this);
        const boundOutbreaksPrevClick = this.onOutbreaksPrevClick.bind(this);
        const boundCountriesNextClick = this.onCountriesNextClick.bind(this);
        const boundCountriesPrevClick = this.onCountriesPrevClick.bind(this);
        const boundOrganizationsNextClick = this.onOrganizationsNextClick.bind(this);
        const boundOrganizationsPrevClick = this.onOrganizationsPrevClick.bind(this);

        const outbreaks = this.state.outbreaks.length > 0 ? (<div>
            <ModelGrid model="Outbreaks" data={this.state.outbreaks} searchTerm={this.state.term}/>
            <PaginationButtons
                backClick={boundOutbreaksPrevClick}
                forwardClick={boundOutbreaksNextClick}
                pageNumber={this.state.outbreaks_page}
            /></div>) : <p style={{paddingBottom: '10px'}}>No Outbreaks</p>;

        const organizations = this.state.organizations.length > 0 ? (<div><ModelGrid model="Organizations" data={this.state.organizations} searchTerm={this.state.term}/>
                <PaginationButtons
                    backClick={boundOrganizationsPrevClick}
                    forwardClick={boundOrganizationsNextClick}
                    pageNumber={this.state.organizations_page}
                /></div>) : <p style={{paddingBottom: '10px'}}>No Organizations</p>;

        const countries = this.state.countries.length > 0 ? (<div><ModelGrid model="Countries" data={this.state.countries} searchTerm={this.state.term}/>
                <PaginationButtons
                    backClick={boundCountriesPrevClick}
                    forwardClick={boundCountriesNextClick}
                    pageNumber={this.state.countries_page}
                /></div>) : <p style={{paddingBottom: '10px'}}>No Countries</p>;

        return (
            <div className="App">
                <h1 className="mt-4 mb-3" align="left">Search: {this.state.term}<small/></h1>
                <Breadcrumbs parents={this.parents} current={this.current}/>
                <h3>Outbreaks</h3>
                {outbreaks}
                <hr/>
                <h3>Organizations</h3>
                {organizations}
                <hr/>
                <h3>Countries</h3>
                {countries}
            </div>
        );
    }
}

export default SearchPage;