import React, { Component } from 'react';

class SideWidget extends Component {
    render() {
        const { title, items } = this.props;
        return (
            <div className="card mb-4">
                <h5 className="card-header" align="left">{title}</h5>
                <div className="card-body">
                    <div className="row">
                        <div className="col-lg-12">
                            <WidgetItems title={title} items={items}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const WidgetItems = props => {
    let model = props.title.toLowerCase();
    const items = props.items.map(item => {
        switch (model) {
            case 'outbreaks':
            case 'organizations':
            case 'countries':
                return(
                    <li>
                      <div align="left"><a href={'/' + model + '/' + item.id}>{item.Name}</a></div>
                    </li>
                );
            default:
                return(
                    <p align="left">{item}</p>
                );
        }
    });

    return items.length === 0 ? 
        <p>None</p>  :
        (<ul className="col-lg-12">
            {items}
        </ul>);
};

export default SideWidget;