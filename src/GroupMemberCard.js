import React, { Component } from 'react';

class GroupMemberCard extends Component {
  render() {
    return (
      <div className="col-lg-4 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src={this.props.image} alt="" />
          <div className="card-body">
            <h4 className="card-title">{this.props.name}</h4>
            <h6 className="card-subtitle mb-2 text-muted">{this.props.role}</h6>
            <p className="card-text">{this.props.description}</p>
          </div>
          <div className="card-footer">
            <a href="#">{this.props.email}</a>
          </div>
        </div>
      </div>
    );
  }
}

export default GroupMemberCard;