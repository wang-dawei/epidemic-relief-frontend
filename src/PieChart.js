import React, { Component } from 'react';
import * as d3 from 'd3';
import { select }from "d3-selection";

class PieChart extends Component {
    constructor(props) {
        super(props);
        this.createPieChart = this.createPieChart.bind(this);
    }

    componentDidMount() {
        this.createPieChart();
    }

    componentDidUpdate() {
        this.createPieChart();
    }

    createPieChart() {
        const node = this.node;
        const width = this.props.size[0];
        const height = this.props.size[1];
        const data = this.props.data;
        const radius = Math.min(width, height) / 2 * 0.7;

        let arc = d3.arc().outerRadius(this.props.outerRadius).innerRadius(0);
        let pie = d3.pie().value(d => d.value).sort(null);

        const arcLabel = d3.arc().innerRadius(radius).outerRadius(radius);

        const g = select(node)
            .append('g')
            .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

        g.selectAll('path')
            .data(pie(this.props.data))
            .enter()
            .append('path')
            .attr('d', arc)
            .attr('fill', function(d,i) {
                return data[i].color;
            });

        const text = g.selectAll('text')
            .data(pie(this.props.data))
            .enter()
            .append('text')
            .attr("transform", d => `translate(${arcLabel.centroid(d)})`)
            .attr("dy", "0.35em");

        text.append("tspan")
            .attr("x", 0)
            .attr("y", "-0.7em")
            .text(d => (d.data.label + " - " + d.data.value));
    }

    render() {
        return <svg ref={node => this.node = node}
                    width={this.props.size[0]} height={this.props.size[1]}>
        </svg>
    }
}

export default PieChart;