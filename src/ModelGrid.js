import React, {Component} from 'react';
import OutbreakGridItem from './OutbreakGridItem';
import OrganizationGridItem from './OrganizationGridItem';
import CountryGridItem from "./CountryGridItem";

class ModelGrid extends Component {
    render() {
        const { model, data, searchTerm } = this.props;
        if (!data.length) {
            return (
                <div style={{margin: '200px'}}>
                    <h4>No results</h4>
                </div>
            );
        }
        return (
            <GridBody model={model} data={data} searchTerm={searchTerm}/>
        );
    }
}

const GridBody = props => {
    const terms = props.searchTerm.split(" ");
    const gridItems = props.data.map(instance => {
        let gridItem;
        switch (props.model) {
            case 'Outbreaks':
                gridItem = <OutbreakGridItem link={'/outbreaks/' + instance.id} title={instance.Disease + ' in ' + instance.Location}
                                             description={instance.Description} disease={instance.Disease}
                                             country={instance.Location} datePosted={instance.StartDate}
                                             transmission={instance.Transmission} altNames={instance.AltNames}
                                             searchTerm={terms}/>;
                break;
            case 'Organizations':
                gridItem = <OrganizationGridItem link={'/organizations/' + instance.id} title={instance.Name}
                                                 mission={instance.Mission} imageUrl={instance.Image}
                                                 locations={instance.AreasServed} website={instance.Link}
                                                 NTEECode={instance.NTEECode} profileLevel={instance.ProfileLevel}
                                                 searchTerm={terms}/>;
                break;
            case 'Countries':
                gridItem = <CountryGridItem link={'/countries/' + instance.Code} title={instance.Name}
                                            population={instance.Population} mortality={instance.Mortality}
                                            lifeExp={instance.LifeExpectancy} flagUrl={instance.Flag}
                                            region={instance.Region} subregion={instance.Subregion}
                                            capital={instance.Capital} searchTerm={terms}/>;
                break;
        }
        return (
            gridItem
        );
    });

    return <div className="row">{gridItems}</div>
};

export default ModelGrid;
