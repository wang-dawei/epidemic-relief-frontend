import React, { Component } from 'react';
import axios from 'axios';
import Select from 'react-select';

import Breadcrumbs from './Breadcrumbs';
import ModelGrid from './ModelGrid';
import PaginationButtons from './PaginationButtons';
import SearchBox from "./SearchBox";


class Organizations extends Component {
    parents = ['Home'];
    current = 'Organizations';
    perPage = 9;

    areaOptions = [
      { value: '', label: 'Area Served'},
      { value: 'Africa', label: 'Africa'},
      { value: 'Americas', label: 'Americas'},
      { value: 'Asia', label: 'Asia'},
      { value: 'Europe', label: 'Europe'},
      { value: 'Oceania', label: 'Oceania'}
    ];
    profileLevelOptions = [
      { value: '', label: 'Profile Level'},
      { value: 'Bronze', label: 'Bronze'},
      { value: 'Silver', label: 'Silver'},
      { value: 'Gold', label: 'Gold'},
      { value: 'Platinum', label: 'Platinum'}
    ];
    sortOptions = [
      { value: '', label: 'Sort' },
      { value: 'asc-name', label: 'Name A-Z' },
      { value: 'dsc-name', label: 'Name Z-A' },
      { value: 'asc-level', label: 'Profile Level A-Z' },
      { value: 'dsc-level', label: 'Profile Level Z-A' }
    ];

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            organizations: [],
            searchTerm: '',
            areaFilter: '',
            profileLevelFilter: '',
            sortby: ''
        };
        this.handleSearch = this.handleSearch.bind(this);
        this.areaFilter = this.areaFilter.bind(this);
        this.profileLevelFilter = this.profileLevelFilter.bind(this);
        this.onSort = this.onSort.bind(this);
        this.clearAll = this.clearAll.bind(this);
    }

    getOrgInfo(pageNumber) {
        if (pageNumber < 1) {
            return;
        }
        const url = this.buildUrl(pageNumber);
        axios.get(url).then(response => {
            const newOrganizations = response.data.map(organization => {
                return {
                    id: organization.org_id,
                    Name: organization.name,
                    Mission: organization.mission,
                    Image: organization.image,
                    AreasServed: organization["areas served"],
                    Link: organization.link,
                    ProfileLevel: organization["profile level"],
                    NTEECode: organization.ntee
                };
            });

            if (newOrganizations.length > 0 || pageNumber === 1) {
                this.setState({
                    page: pageNumber,
                    organizations: newOrganizations
                });
            }
        });
    }

    buildUrl(pageNumber) {
        let url = 'http://api.epidemicrelief.me/organizations?page=' + pageNumber + '&per_page=' + this.perPage;
        if (!!this.state.areaFilter) {
            url += '&area=' + this.state.areaFilter;
        }
        if (!!this.state.profileLevelFilter) {
            url += '&profile_level=' + this.state.profileLevelFilter;
        }
        if (!!this.state.sortby) {
            url += '&sort=' + this.state.sortby;
        }
        if (!!this.state.searchTerm) {
            url += '&search=' + this.state.searchTerm.replace(' ', '%20');
        }
        return url;
    }

    onNextClick() {
        this.getOrgInfo(this.state.page + 1);
        console.log('click forward');
    }

    onPrevClick() {
        this.getOrgInfo(this.state.page - 1);
        console.log('click back');
    }

    componentDidMount() {
        this.getOrgInfo(1);
    }

    handleSearch(term) {
        this.setState(
            {searchTerm: term},
            function () {
                this.getOrgInfo(1);
            }
        );
    }

    areaFilter(e) {
        this.setState(
          {areaFilter: e.value},
          function () {
              this.getOrgInfo(1);
          }
        )
    }

    profileLevelFilter(e) {
        this.setState(
          {profileLevelFilter: e.value},
          function () {
              this.getOrgInfo(1);
          }
        )
    }

    onSort(e) {
        this.setState(
          {sortby: e.value},
          function () {
              this.getOrgInfo(1);
          }
        )
    }

    clearAll() {
        if (!!this.state.searchTerm || !!this.state.areaFilter || !!this.state.profileLevelFilter || !!this.state.sortby) {
            window.location.reload();
        }
    }

    render() {
        const boundNextClick = this.onNextClick.bind(this);
        const boundPrevClick = this.onPrevClick.bind(this);
        return (
            <div className="App">
                <h1 className="mt-4 mb-3" align="left">{this.current}
                    <small/>
                </h1>
                <div className="row col-lg-12">
                    <Select className="col-lg-2" name="Areas Served" placeholder="Area Served" defaultValue=""
                            options={this.areaOptions} onChange={this.areaFilter}/>
                    <Select className="col-lg-2" name="Profile Level" placeholder="Profile Level" defaultValue=""
                            options={this.profileLevelOptions} onChange={this.profileLevelFilter}/>
                    <Select className="col-lg-2" name="Sort" placeholder="Sort" defaultValue=""
                            options={this.sortOptions} onChange={this.onSort}/>

                    <SearchBox onSearch={this.handleSearch}/>
                    <button type="button" className="btn btn-danger" style={{marginLeft: '32px'}} onClick={this.clearAll}>Clear</button>
                </div>
                <Breadcrumbs parents={this.parents} current={this.current}/>
                <ModelGrid model={this.current} data={this.state.organizations} searchTerm={this.state.searchTerm}/>

                {this.state.organizations.length > 0 ? <PaginationButtons
                    backClick={boundPrevClick}
                    forwardClick={boundNextClick}
                    pageNumber={this.state.page}
                /> : null}
            </div>
        );
    }
}

export default Organizations;