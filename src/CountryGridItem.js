import React, { Component } from 'react';
import Highlighter from 'react-highlight-words';

class CountryGridItem extends Component {
    render() {
        const { link, title, population, mortality, lifeExp, flagUrl, region, subregion, searchTerm} = this.props;
        return (
            <div className="col-lg-4 col-sm-6 portfolio-item">
                <div className="card h-100">
                    <a href={link}><img className="card-img-top" src={flagUrl} alt={title} style={{maxHeight: '200px',
                        maxWidth: '250px', width: 'auto', padding: '10px'}}/></a>
                    <div className="card-body">
                        <h4 className="card-title">
                            <a href={link}>
                              <Highlighter searchWords={searchTerm} textToHighlight={title}/>
                            </a>
                        </h4>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Region: ' + region}/>
                        </p>
                        {/*<p className="card-text" align="left">Region: {region}</p>*/}
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'SubRegion: ' + subregion}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Population: ' + population}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Mortality Rate (per 100,000 people): ' + mortality}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Life Expectancy: ' + lifeExp + ' years'}/>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default CountryGridItem;
