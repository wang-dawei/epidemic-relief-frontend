import React, { Component } from 'react';

import Breadcrumbs from './Breadcrumbs';
import GitLabStatsTable from './GitLabStatsTable';
import GroupMemberCard from './GroupMemberCard';

import team from './images/team.jpg';
import atsuki from './images/Atsuki.jpg';
import david from './images/david.jpg';
import kevin from './images/kevin.jpg';
import pranay from './images/Pranay.jpg';
import vishal from './images/vishal.png';

class About extends Component {
  render() {
    const bioData = [
      {
        image: vishal,
        name: "Vishal Gullapalli",
        role: "Back-end Developer",
        description: "Fourth Year Computer Science student at UT. Likes video games, comic books, and football.",
        email:"vishal.gullapalli@gmail.com",
      },
      {
        image: pranay,
        name: "Pranay Nakirekanti",
        role: "Full-stack Developer",
        description: "I'm currently a Senior at the University of Texas at Austin, where I'm pursuing Computer Science and Neuroscience degrees. I enjoy developing software and technology, and hope to build something that other people can use too. When I'm not working on my computer, you'll find me spending time on hobbies such as cooking, playing the piano, running the occasional 5K, and playing basketball.",
        email: "pranaynaki@gmail.com",
      },
      {
        image: atsuki,
        name: "Atsuki Sawada",
        role: "Back-end Developer",
        description: "Third-year computer science exchange student from Japan.",
        email: "asawada729@gmail.com",
      },
      {
        image: david,
        name: "David Wang",
        role: "Full-stack Developer",
        description: "Fourth-year CS major at UT. I like playing Ultimate Frisbee!",
        email: "davidwang@utexas.edu",
      },
      {
        image: kevin,
        name: "Kevin Wang",
        role: "Front-end Developer",
        description: "Third-year computer science major at UT. Big fan of dog pictures.",
        email: "kev.w.713@gmail.com",
      },
    ];
    const bioComponents = bioData.map((props) => {
      const {description, email, image, name, role} = props;
      return <GroupMemberCard
        description={description}
        email={email}
        image={image}
        name={name}
        role={role}
      />;
    });
    const postmanURL = "https://documenter.getpostman.com/view/5487483/RWgm41ZV";
    const backendRepoURL = "https://gitlab.com/asawada729/idb-project";
    const frontendRepoURL = "https://gitlab.com/wang-dawei/epidemic-relief-frontend";
    return (
      <div className="container">
        <h1 className="mt-4 mb-3">About Us
          <small></small>
        </h1>

        <Breadcrumbs parents={['Home']} current="About"  />

        <div className="row">
          <div className="col-lg-6">
            <img className="img-fluid rounded mb-4" src={team} alt="" />
          </div>
          <div className="col-lg-6">
            <h2>About Epidemic Relief</h2>
            <p>We are a group of students dedicated to raising awareness of current epidemics and outbreaks around the world, locations that are affected, and means to help, such as organizations to donate money or time to.</p>
            <a href={postmanURL}>Postman REST API Design</a>
            <br />
            <a href={backendRepoURL}>GitLab back-end repo</a>
            <br />
            <a href={frontendRepoURL}>GitLab front-end repo</a>
            <br />
            <GitLabStatsTable />
          </div>
        </div>

        <h2>Our Team</h2>
        <div className="row">
          {bioComponents}
        </div>
        <hr/>
        <h2>Tools</h2>
          <p>Markup and Styling: HTML5, CSS3, Bootstrap.css</p>
          <p>Scripting: Javascript, Bootstrap.js, ReactJS</p>
          <p>Database: PostgreSQL</p>
          <p>Backend server framework and database queries: Flask, SQLAlchemy</p>
          <p>Hosting: Google Cloud Platform</p>
          <p>Containers: Docker</p>
          <p>Version Control: GitLab</p>
        <hr/>
        <h2>Data Sources</h2>
        <h5 style={{margin: '5px'}}>Outbreaks</h5>
          <a href={'https://tools.cdc.gov/api/v2/resources/media/'}>Centers for Disease Control and Prevention (CDC)</a>
          <br />
          <a href={'https://brd.bsvgateway.org/api'}>Biosurveillance Resource Directory (BRD)</a>
          <br />
          <a href={'https://devcenter-square.github.io/disease-info'}>Devcenter Square - Disease Info API</a>
          <br />
        <h5 style={{margin: '5px'}}>Countries</h5>
          <a href={'https://restcountries.eu/'}>REST Countries</a>
          <br />
          <a href={'http://apps.who.int/gho/data/node.resources.api'}>World Health Organization</a>
          <br />
        <h5 style={{margin: '5px'}}>Organizations</h5>
          <a href={'https://learn.guidestar.org/products/business-solutions/guidestar-apis'}>Guidestar</a>
          <br />
        <hr/>
      </div>
    );
  }
}

export default About;
