import React, { Component } from 'react';

class Breadcrumbs extends Component {
    render() {
        const {parents, current} = this.props;
        return(
            <div>
                <h1 className="mt-4 mb-3"/>
                <Crumbs parents={parents} current={current}/>
            </div>
        );
    }
}

const Crumbs = props => {
    const crumbs = props.parents.map(parent => {
        let pageRef;
        switch (parent) {
            case 'Home':
                pageRef = '/';
                break;
            case 'Outbreaks':
                pageRef = '/outbreaks';
                break;
            case 'Organizations':
                pageRef = '/organizations';
                break;
            case 'Countries':
                pageRef = '/countries';
                break;
        }
        return (
            <li className="breadcrumb-item">
                <a href={pageRef}>{parent}</a>
            </li>
        );
    });

    return (
        <ol className="breadcrumb">
            {crumbs}
            <li className="breadcrumb-item active">{props.current}</li>
        </ol>
    );
};

export default Breadcrumbs;
