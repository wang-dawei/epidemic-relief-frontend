import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import OutbreakGridItem from './OutbreakGridItem';
import Outbreaks from './Outbreaks';

const assert = require('assert');

describe('Test Suite', function() {
  it('App renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('Tests griditem rendering and props', () => {
    const div = document.createElement('div');
    const item = <OutbreakGridItem link={'/outbreaks/' + '544'} title={'Malaria' + ' in ' + 'Jarjarbinksistan'}
                                             description={'a deadly virus'} disease={'malaria'}
                                             country={'Jarjarbinksistan'} datePosted={'05-03-2018'}
                                             transmission={'air'} altNames={''}
                                             searchTerm={[]}/>;
    ReactDOM.render(item, div);
    assert(item.props.description == 'a deadly virus');
    ReactDOM.unmountComponentAtNode(div);
  });
  it('Tests basic rendering of a model page and model state initializations', () => {
    const div = document.createElement('div');
    const model = ReactDOM.render(<Outbreaks />, div);
    assert(model.state.page == 1);
    ReactDOM.unmountComponentAtNode(div);
  });

});