import React, { Component } from 'react';

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateTerm = this.updateTerm.bind(this);
    this.state = {
        term: ''
    }
  }

  updateTerm(e) {
    this.setState({term: e.target.value});
  }

  handleSubmit(e) {
    this.props.onSearch(this.state.term);
  }

  render() {
    return (
      <form className="form-inline justify-content-center">
        <div className="form-group" style={{marginRight: '4px', marginLeft: '16px', padding: '0px'}}>
          <input type="text" className="form-control" id="searchBox" placeholder="Search..." onChange={this.updateTerm}
                 onKeyPress={e => {
                   if (e.key === 'Enter') {
                     e.preventDefault();
                   }
                 }}
                 style={{backgroundColor: '#fff', color: '#343a40', maxWidth: '200px'}}/>
        </div>
        <button className="btn btn-secondary" id="searchButton" type="button" onClick={this.handleSubmit}>Go</button>
      </form>
    )
  }
}

export default SearchBox;