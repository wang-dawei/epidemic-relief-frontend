import React, { Component } from 'react';
import axios from 'axios';
import Select from 'react-select';

import Breadcrumbs from './Breadcrumbs';
import ModelGrid from './ModelGrid';
import NumericalFilter from './NumericalFilter';
import PaginationButtons from './PaginationButtons';
import SearchBox from './SearchBox';


class Countries extends Component {
    parents = ['Home'];
    current = 'Countries';
    perPage = 9;

    regionOptions = [
        { value: '', label: 'Region' },
        { value: 'Africa', label: 'Africa' },
        { value: 'Americas', label: 'Americas' },
        { value: 'Asia', label: 'Asia' },
        { value: 'Europe', label: 'Europe' },
        { value: 'Oceania', label: 'Oceania' }
    ];
    subregionOptions = [
        { value: '', label: 'Subregion' },
        { value: 'Australia and New Zealand', label: 'Australia and New Zealand' },
        { value: 'Caribbean', label: 'Caribbean' },
        { value: 'Central America', label: 'Central America' },
        { value: 'Central Asia', label: 'Central Asia' },
        { value: 'Eastern Africa', label: 'Eastern Africa' },
        { value: 'Eastern Asia', label: 'Eastern Asia' },
        { value: 'Eastern Europe', label: 'Eastern Europe' },
        { value: 'Melanesia', label: 'Melanesia' },
        { value: 'Micronesia', label: 'Micronesia' },
        { value: 'Middle Africa', label: 'Middle Africa' },
        { value: 'Northern Africa', label: 'Northern Africa' },
        { value: 'Northern America', label: 'Northern America' },
        { value: 'Northern Europe', label: 'Northern Europe' },
        { value: 'Polynesia', label: 'Polynesia' },
        { value: 'South America', label: 'South America' },
        { value: 'South-Eastern Asia', label: 'South-Eastern Asia' },
        { value: 'Southern Africa', label: 'Southern Africa' },
        { value: 'Southern Asia', label: 'Southern Asia' },
        { value: 'Southern Europe', label: 'Southern Europe' },
        { value: 'Western Africa', label: 'Western Africa' },
        { value: 'Western Asia', label: 'Western Asia' },
        { value: 'Western Europe', label: 'Western Europe' }
    ];
    sortOptions = [
        { value: '', label: 'Sort' },
        { value: 'asc-name', label: 'Name A-Z' },
        { value: 'dsc-name', label: 'Name Z-A' },
        { value: 'asc-region', label: 'Region A-Z' },
        { value: 'dsc-region', label: 'Region Z-A' },
        { value: 'asc-subregion', label: 'Subregion A-Z' },
        { value: 'dsc-subregion', label: 'Subregion Z-A' },
        { value: 'asc-population', label: 'Population Asc' },
        { value: 'dsc-population', label: 'Population Desc' },
        { value: 'asc-area', label: 'Area A-Z' },
        { value: 'dsc-area', label: 'Area Z-A' },
        { value: 'asc-LifeExpectancy', label: 'LifeExpectancy Asc' },
        { value: 'dsc-LifeExpectancy', label: 'LifeExpectancy Desc' },
        { value: 'asc-mortality', label: 'Mortality Asc' },
        { value: 'dsc-mortality', label: 'Mortality Desc' }
    ];
    filterOptions = [
        { value: '', label: 'Filter by value' },
        { value: 'min-', label: 'Name A-Z' },
        { value: 'dsc-name', label: 'Name Z-A' }
    ];

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            countries: [],
            searchTerm: '',
            mortality_min: '',
            mortality_max: '',
            life_expectancy_min: '',
            life_expectancy_max: '',
            population_min: '',
            population_max: '',
            regionFilter: '',
            subregionFilter: '',
            sortby: ''
        };
        this.handleSearch = this.handleSearch.bind(this);
        this.regionFilter = this.regionFilter.bind(this);
        this.subregionFilter = this.subregionFilter.bind(this);
        this.onSort = this.onSort.bind(this);
        this.updateFilters = this.updateFilters.bind(this);
        this.clearAll = this.clearAll.bind(this);
    }
    getCountriesInfo(pageNumber) {
        if (pageNumber < 1) {
            return;
        }
        const url = this.buildUrl(pageNumber);
        axios.get(url).then(response => {
            const newCountries = response.data.map(country => {
                return {
                    Code: country.code,
                    Capital: country.capital,
                    Region: country.region,
                    Subregion: country.subregion,
                    Name: country.name,
                    LifeExpectancy: country["life expectancy"],
                    Mortality: country.mortality,
                    Population: country.population,
                    Flag: country.flag,
                    Area: country.area
                    }
            });

            if (newCountries.length > 0 || pageNumber === 1) {
                this.setState({
                    page: pageNumber,
                    countries: newCountries
                });
            }
        })
    }

    buildUrl(pageNumber) {
        let url = 'http://api.epidemicrelief.me/countries?page=' + pageNumber + '&per_page=' + this.perPage;
        if (!!this.state.regionFilter) {
            url += '&region=' + this.state.regionFilter;
        }
        if (!!this.state.subregionFilter) {
            url += '&subregion=' + this.state.subregionFilter;
        }
        if (!!this.state.population_min) {
            url += '&population_min=' + this.state.population_min;
        }
        if (!!this.state.population_max) {
            url += '&population_max=' + this.state.population_max;
        }
        if (!!this.state.mortality_min) {
            url += '&mortality_min=' + this.state.mortality_min;
        }
        if (!!this.state.mortality_max) {
            url += '&mortality_max=' + this.state.mortality_max;
        }
        if (!!this.state.life_expectancy_min) {
            url += '&life_expectancy_min=' + this.state.life_expectancy_min;
        }
        if (!!this.state.life_expectancy_max) {
            url += '&life_expectancy_max=' + this.state.life_expectancy_max;
        }
        if (!!this.state.sortby) {
            url += '&sort=' + this.state.sortby;
        }
        if (!!this.state.searchTerm) {
            url += '&search=' + this.state.searchTerm.replace(' ', '%20');
        }
        console.log(url)
        return url;
    }

    onNextClick() {
        this.getCountriesInfo(this.state.page + 1);
    }

    onPrevClick() {
        this.getCountriesInfo(this.state.page - 1);
    }

    componentDidMount() {
        this.getCountriesInfo(1);
    }

    handleSearch(term) {
        this.setState(
            {searchTerm: term},
            function () {
                this.getCountriesInfo(1);
            }
        );
    }

    regionFilter(e) {
        this.setState(
          {regionFilter: e.value},
          function () {
              this.getCountriesInfo(1);
          }
        )
    }

    subregionFilter(e) {
        this.setState(
          {subregionFilter: e.value},
          function () {
              this.getCountriesInfo(1);
          }
        )
    }

    onSort(e) {
        this.setState(
            {sortby: e.value},
            function () {
                this.getCountriesInfo(1);
            }
        )
    }

    updateFilters(e) {
        var a = {};
        switch (e.target.id) {
            case "popmin":
                a = {population_min: e.target.value};
                break;
            case "popmax":
                a = {population_max: e.target.value};
                break;
            case "mortmin":
                a = {mortality_min: e.target.value};
                break;
            case "mortmax":
                a = {mortality_max: e.target.value};
                break;
            case "lifemin":
                a = {life_expectancy_min: e.target.value};
                break;
            case "lifemax":
                a = {life_expectancy_max: e.target.value};
                break;
        }
        this.setState(
            a,
            function () {
                this.getCountriesInfo(1);
            }
        )
    }

    clearAll() {
        if (!!this.state.searchTerm || !!this.state.mortality_min || !!this.state.mortality_max ||
            !!this.state.life_expectancy_min || !!this.state.life_expectancy_max || !!this.state.population_min ||
            !!this.state.population_max || !!this.state.regionFilter || !!this.state.subregionFilter ||
            !!this.state.sortby) {
            window.location.reload();
        }
    }

    render() {
        const boundNextClick = this.onNextClick.bind(this);
        const boundPrevClick = this.onPrevClick.bind(this);
        const boundUpdateFilters = this.updateFilters.bind(this);
        return (
            <div className="App">
                <h1 className="mt-4 mb-3" align="left">{this.current}
                    <small/>
                </h1>
                <div className="col-lg-12 row">
                    <Select className="col-lg-2" name="Region" placeholder="Region" defaultValue=""
                            options={this.regionOptions} onChange={this.regionFilter}/>
                    <Select className="col-lg-2" name="Subregion" placeholder="Subregion" defaultValue=""
                            options={this.subregionOptions} onChange={this.subregionFilter}/>
                    <Select className="col-lg-2" name="Sort" placeholder="Sort" defaultValue=""
                            options={this.sortOptions} onChange={this.onSort}/>                    

                    <SearchBox onSearch={this.handleSearch}/>
                    <button type="button" className="btn btn-danger" style={{marginLeft: '32px'}} onClick={this.clearAll}>Clear</button>
                </div>
                <br/>
                <div>
                    <form style={{textAlign: "left"}}>
                        <table>
                            <NumericalFilter 
                                highId="popmax"
                                highPlaceholder="high"
                                label="Filter by Population"
                                lowId="popmin"
                                lowPlaceholder="low"
                                onChange={boundUpdateFilters}
                            />
                            <NumericalFilter 
                                highId="mortmax"
                                highPlaceholder="high"
                                label="Filter by Mortality Rate"
                                lowId="mortmin"
                                lowPlaceholder="low"
                                onChange={boundUpdateFilters}
                            />
                            <NumericalFilter 
                                highId="lifemax"
                                highPlaceholder="high"
                                label="Filter by Life Expectancy"
                                lowId="lifemin"
                                lowPlaceholder="low"
                                onChange={boundUpdateFilters}
                            />
                        </table>
                    </form>
                </div>
                <Breadcrumbs parents={this.parents} current={this.current}/>
                <ModelGrid model={this.current} data={this.state.countries} searchTerm={this.state.searchTerm}/>

                {this.state.countries.length > 0 ? <PaginationButtons
                    backClick={boundPrevClick}
                    forwardClick={boundNextClick}
                    pageNumber={this.state.page}
                /> : null}
            </div>
        );
    }
}

export default Countries;