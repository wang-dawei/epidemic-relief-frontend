import React, { Component } from 'react';
import Highlighter from 'react-highlight-words';

class OutbreakGridItem extends Component {
    getAsString(str) {
        str = str.replace('[', '');
        str = str.replace(']', '');
        str = str.replace(/'/gi, '');
        return str;
    }

    render() {
        const { link, title, description, disease, country, datePosted, searchTerm, transmission, altNames} = this.props;
        return (
            <div className="col-lg-4 col-sm-6 portfolio-item">
                <div className="card h-100">
                    <div className="card-body">
                        <h4 className="card-title">
                            <a href={link}>
                                <Highlighter searchWords={searchTerm} textToHighlight={title}/>
                            </a>
                        </h4>
                        <p className="card-text short-description" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={description}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Disease: ' + disease}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Transmission: ' + this.getAsString(transmission)}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Country: ' + country}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Alternate Names: ' + this.getAsString(altNames)}/>
                        </p>
                        <p className="card-text" align="left">
                          <Highlighter searchWords={searchTerm} textToHighlight={'Date Posted: ' + datePosted}/>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default OutbreakGridItem;
