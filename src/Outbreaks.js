import React, { Component } from 'react';
import axios from 'axios';

import Breadcrumbs from './Breadcrumbs';
import ModelGrid from './ModelGrid';
import NumericalFilter from './NumericalFilter';
import PaginationButtons from './PaginationButtons';
import SearchBox from "./SearchBox";
import Select from 'react-select';

class Outbreaks extends Component {
    parents = ['Home'];
    current = 'Outbreaks';
    perPage = 9;

    countryOptions = [
        { value: '', label: 'Country Filter'},
        { value: 'France', label: 'France' },
        { value: 'Greece', label: 'Greece' },
        { value: 'Italy', label: 'Italy' },
        { value: 'Japan', label: 'Japan'},
        { value: 'Nigeria', label: 'Nigeria' },
        { value: 'Papua New Guinea', label: 'Papua New Guinea' },
        { value: 'Romania', label: 'Romania' },
        { value: 'Serbia', label: 'Serbia'},
        { value: 'Somalia', label: 'Somalia' },
        { value: 'Ukraine', label: 'Ukraine' },
        { value: 'Yemen', label: 'Yemen'}
    ];
    diseaseOptions = [
      { value: '', label: 'Disease Filter'},
      { value: 'Cholera', label: 'Cholera'},
      { value: 'Measles', label: 'Measles'},
      { value: 'Monkeypox', label: 'Monkeypox'},
      { value: 'Poliomyelitis', label: 'Poliomyelitis'},
      { value: 'Rubella', label: 'Rubella'}
    ];
    sortOptions = [
      { value: '', label: 'Sort'},
      { value: 'asc-disease', label: 'Disease A-Z'},
      { value: 'dsc-disease', label: 'Disease Z-A'},
      { value: 'asc-location', label: 'Location A-Z'},
      { value: 'dsc-location', label: 'Location Z-A'}
    ];

    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            outbreaks: [],
            searchTerm: '',
            countryFilter: '',
            diseaseFilter: '',
            start_date_min: '',
            start_date_max: '',
            sortby: ''
        };
        this.handleSearch = this.handleSearch.bind(this);
        this.countryFilter = this.countryFilter.bind(this);
        this.diseaseFilter = this.diseaseFilter.bind(this);
        this.onSort = this.onSort.bind(this);
        this.updateFilters = this.updateFilters.bind(this);
        this.clearAll = this.clearAll.bind(this);
    }

    getOutbreakData(pageNumber) {
        if (pageNumber < 1) {
            return;
        }
        const url = this.buildUrl(pageNumber);
        console.log("url: " + url);
        axios.get(url).then(response => {
            const newOutbreaks = response.data.map(outbreak => {
                return {
                    id: outbreak.outbreak_id,
                    Description: outbreak.description,
                    Disease: outbreak.disease,
                    Location: outbreak.country,
                    StartDate: outbreak.date,
                    Transmission: outbreak.transmission,
                    AltNames: outbreak["alternate names"]
                };
            });

            if (newOutbreaks.length > 0 || pageNumber === 1) {
                this.setState({
                    page: pageNumber,
                    outbreaks: newOutbreaks
                });
            }
        });
    }

    buildUrl(pageNumber) {
        let url = 'http://api.epidemicrelief.me/outbreaks?page=' + pageNumber + '&per_page=' + this.perPage;
        if (!!this.state.countryFilter) {
            url += '&country=' + this.state.countryFilter;
        }
        if (!!this.state.diseaseFilter) {
            url += '&disease_name=' + this.state.diseaseFilter;
        }
        if (!!this.state.start_date_min) {
            url += '&start_date_min=' + this.state.start_date_min;
        }
        if (!!this.state.start_date_max) {
            url += '&start_date_max=' + this.state.start_date_max;
        }
        if (!!this.state.sortby) {
            url += '&sort=' + this.state.sortby;
        }
        if (!!this.state.searchTerm) {
            url += '&search=' + this.state.searchTerm.replace(' ', '%20');
        }
        return url;
    }

    onNextClick() {
        this.getOutbreakData(this.state.page + 1);
    }

    onPrevClick() {
        this.getOutbreakData(this.state.page - 1);
    }

    componentDidMount() {
        this.getOutbreakData(1);
    }

    handleSearch(term) {
        this.setState(
            {searchTerm: term},
            function () {
                this.getOutbreakData(1);
            }
        );
    }

    countryFilter(e) {
        this.setState(
            {countryFilter: e.value},
            function () {
                console.log('countryFilter: ' + this.state.countryFilter);
                this.getOutbreakData(1);
        });
    }

    diseaseFilter(e) {
        this.setState(
            {diseaseFilter: e.value},
            function () {
                console.log('diseaseFilter: ' + this.state.diseaseFilter);
                this.getOutbreakData(1);
            }
        )
    }

    onSort(e) {
        this.setState(
          {sortby: e.value},
          function () {
              this.getOutbreakData(1);
          }
        )
    }
    updateFilters(e) {
        let a = {};
        switch (e.target.id) {
            case "datemin":
                a = {start_date_min: e.target.value};
                break;
            case "datemax":
                a = {start_date_max: e.target.value};
                break;
        }
        this.setState(
            a,
            function () {
                this.getOutbreakData(1);
            }
        )
    }

    clearAll() {
        if (!!this.state.searchTerm || !!this.state.countryFilter || !!this.state.diseaseFilter ||
            !!this.state.start_date_min || !!this.state.start_date_max || !!this.state.sortby) {
            window.location.reload();
        }
    }

    render() {
        const boundNextClick = this.onNextClick.bind(this);
        const boundPrevClick = this.onPrevClick.bind(this);
        const boundUpdateFilters = this.updateFilters.bind(this);
        return (
            <div className="App">
                <h1 className="mt-4 mb-3" align="left">{this.current}
                    <small/>
                </h1>
                <div className="row col-lg-12">
                    <Select className="col-lg-2" name="Country" placeholder="Country" defaultValue=""
                            options={this.countryOptions} onChange={this.countryFilter}/>
                    <Select className="col-lg-2" name="Disease" placeholder="Disease" defaultValue=""
                            options={this.diseaseOptions} onChange={this.diseaseFilter}/>
                    <Select className="col-lg-2" name="Sort" placeholder="Sort" defaultValue=""
                            options={this.sortOptions} onChange={this.onSort}/>

                    <SearchBox onSearch={this.handleSearch}/>
                    <button type="button" className="btn btn-danger" style={{marginLeft: '32px'}} onClick={this.clearAll}>Clear</button>
                </div>
                <br/>
                <div>
                    <form style={{textAlign: "left"}}>
                        <table>
                            <NumericalFilter 
                                highId="datemax"
                                highPlaceholder="MM-DD-YYYY"
                                label="Filter by Date Posted"
                                lowId="datemin"
                                lowPlaceholder="MM-DD-YYYY"
                                onChange={boundUpdateFilters}
                            />
                        </table>
                    </form>
                </div>
                <Breadcrumbs parents={this.parents} current={this.current}/>
                <ModelGrid model={this.current} data={this.state.outbreaks} searchTerm={this.state.searchTerm}/>

                {this.state.outbreaks > 0 ? <PaginationButtons
                    backClick={boundPrevClick}
                    forwardClick={boundNextClick}
                    pageNumber={this.state.page}
                /> : null}
            </div>
        );
    }
}

export default Outbreaks;
