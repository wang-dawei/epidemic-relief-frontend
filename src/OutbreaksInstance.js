import React, { Component } from 'react';
import Breadcrumbs from './Breadcrumbs';
import SideWidget from './SideWidget';
import axios from 'axios';

class OutbreaksInstance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            Name: '',
            Disease: '',
            Location: '',
            Description: '',
            Date: '',
            Severity: '',
            Link: '',
            Transmission: '',
            AltNames: '',
            organizations: [],
            countries: [],
        }
    }
    parents = ['Home', 'Outbreaks'];

    getAsString(str) {
        str = str.replace('[', '');
        str = str.replace(']', '');
        str = str.replace(/'/gi, '');
        return str;
    }

    componentDidMount() {
        const url = 'http://api.epidemicrelief.me/outbreaks?outbreak_id=' + this.state.id;
        axios.get(url).then(response => {
            console.log(response.data);
            response.data.forEach(outbreak => {
                this.setState({Name: outbreak.disease + ' in ' + outbreak.country});
                this.setState({Disease: outbreak.disease});
                this.setState({Location: outbreak.country});
                this.setState({Description: outbreak.description});
                this.setState({Date: outbreak.date});
                this.setState({Severity: outbreak.level});
                this.setState({Link: outbreak.link});
                this.setState({Transmission: this.getAsString(outbreak.transmission)});
                this.setState({AltNames: this.getAsString(outbreak["alternate names"])});
            });
            this.getOrganizations();
            this.getCountries();
        });
    }

    getOrganizations() {
        const url = 'http://api.epidemicrelief.me/organizations';
        axios.get(url + '?outbreak_id=' + this.state.id).then(response => {
            const newOrganizations = response.data.map(organization => {
                return {
                    id: organization.org_id,
                    Name: organization.name
                };
            });
            this.setState({organizations: newOrganizations});
        });
    }

    getCountries() {
        const url = 'http://api.epidemicrelief.me/countries';
        axios.get(url + '?name=' + this.state.Location).then(response => {
            const newCountries = response.data.map(country => {
                return {
                    id: country.code,
                    Name: country.name
                };
            });
            this.setState({countries: newCountries});
        });
    }

    render() {
        return (
            <div className="App">
                <h1 className="mt-4 mb-3" align="left">{this.state.Name}
                    <small/>
                </h1>
                <Breadcrumbs parents={this.parents} current={this.state.Name}/>

                <div className="row">
                    <div className="col-lg-8">
                        <p className="lead" align="left">General Info:</p>
                        <table style={{width: '100%'}}>
                            <tr>
                                <td align="left">Disease:</td>
                                <td align="left">{this.state.Disease}</td>
                            </tr>
                            <tr>
                                <td align="left">Alternate Names:</td>
                                <td align="left">{this.getAsString(this.state.AltNames)}</td>
                            </tr>
                            <tr>
                                <td align="left">Transmission:</td>
                                <td align="left">{this.state.Transmission}</td>
                            </tr>
                            <tr>
                                <td align="left">Affected Locations:</td>
                                <td align="left">{this.state.Location}</td>
                            </tr>
                            <tr>
                                <td align="left">Severity Type:</td>
                                <td align="left">{this.state.Severity}</td>
                            </tr>
                            <tr>
                                <td align="left">Date Posted:</td>
                                <td align="left">{this.state.Date}</td>
                            </tr>
                        </table>
                        <hr/>

                        <p className="lead" align="left">Description:</p>
                        <p align="left">{this.state.Description}</p>
                        <hr/>

                        <p className="lead" align="left">CDC Link:</p>
                        <div align="left"><a href={this.state.Link} target="_blank">{this.state.Link}</a></div>

                        <hr/>
                    </div>

                    <div className="col-md-4">
                        <SideWidget title="Organizations" items={this.state.organizations}/>
                        <SideWidget title="Countries" items={this.state.countries}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default OutbreaksInstance;