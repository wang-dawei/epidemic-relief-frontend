import React, {Component} from 'react';
import liquid_in_tubes_jpg from './images/liquid_in_tubes.jpg';
import organization_jpg from './images/organization.jpg';
import map_jpg from './images/map.jpg';

class Carousel extends Component {
    render() {
        return (
            <header>
                <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"/>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"/>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"/>
                    </ol>
                    <div className="carousel-inner" role="listbox">
                        <div className="carousel-item active">
                            <img src={liquid_in_tubes_jpg} alt="liquid_in_tubes"/>
                                <div className="carousel-caption d-none d-md-block">
                                    <h3>Disease Outbreaks</h3>
                                    <p/>
                                </div>
                        </div>
                        <div className="carousel-item">
                            <img src={organization_jpg} alt="organization"/>
                                <div className="carousel-caption d-none d-md-block">
                                    <h3>Organizations</h3>
                                    <p/>
                                </div>
                        </div>
                        <div className="carousel-item">
                            <img src={map_jpg} alt="map"/>
                                <div className="carousel-caption d-none d-md-block">
                                    <h3>Locations</h3>
                                    <p/>
                                </div>
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                       data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"/>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button"
                       data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"/>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
            </header>
        );
    }
}

export default Carousel;
