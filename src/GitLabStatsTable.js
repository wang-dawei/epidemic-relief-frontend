import React, { Component } from 'react';
import axios from 'axios';

import './GitLabTable.css'

class GitLabStatsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commitCount: [0, 0, 0, 0, 0],
      issueCount: [0, 0, 0, 0, 0],
      unitTestCount: [0, 0, 0, 0, 0],
    };
  }
  getCommitData() {
    var commitCount = [0, 0, 0, 0, 0];
    const urls = ["https://gitlab.com/api/v4/projects/8592364/repository/commits?per_page=100",
                  "https://gitlab.com/api/v4/projects/9018008/repository/commits?per_page=100"];
    urls.forEach((url) => {
      axios.get(url).then(res => {
        res.data.forEach(function (commit) {
          switch (commit["author_email"]) {
            case ("vishal.gullapalli@gmail.com"):
              commitCount[0]++;
              break;
            case ("pranaynaki@gmail.com"):
              commitCount[1]++;
              break;
            case ("asawada729@AtsukisPC.localdomain"):
            case ("asawada729@gmail.com"):
              commitCount[2]++;
              break;
            case ("davidwang@utexas.edu"):
              commitCount[3]++;
              break;
            case ("kev.w.713@utexas.edu"):
              commitCount[4]++;
              break;
            default:
          }
        });
        this.setState({commitCount: commitCount});
      });
    });
  }

  getIssueData() {
    var issueCount = [0, 0, 0, 0, 0];
    const urls = ["https://gitlab.com/api/v4/projects/8592364/issues?state=closed&per_page=100",
                  "https://gitlab.com/api/v4/projects/9018008/issues?state=closed&per_page=100"];
    urls.forEach((url) => {
      axios.get(url).then(res => {
        res.data.forEach(function (issue) {
          var users = issue["assignees"];
          if (users.length === 0){
            users = [issue["author"]];
          }
          users.forEach(user => {
            switch (user["username"]) {
              case ("vgulla"):
                issueCount[0]++;
                break;
              case ("pranaynaki"):
                issueCount[1]++;
                break;
              case ("asawada729"):
                issueCount[2]++;
                break;
              case ("wang-dawei"):
                issueCount[3]++;
                break;
              case ("kevdub"):
                issueCount[4]++;
                break;
              default:
            }
          });
        });
        this.setState({issueCount: issueCount});
      });
    });
  }

  getTestData() {
    var unitTestCount = [0, 0, 0, 0, 0];
    const urls = ["https://gitlab.com/api/v4/projects/8592364/repository/files/tests%2ftests.py/raw?ref=master",
                  "https://gitlab.com/api/v4/projects/9018008/repository/files/tests%2fguitests.py/raw?ref=master",
                  "https://gitlab.com/api/v4/projects/9018008/repository/files/tests%2ftests.js/raw?ref=master",
                  ];
    urls.forEach((url) => {
      axios.get(url).then(res => {
        const lines = res.data.split("\n");
        for (var i = 0; i < 5; i++) {
          const current = lines[i].split(" ");
          switch (current[1]) {
            case ("Vishal:"):
              unitTestCount[0] += parseInt(current[2]);
              break;
            case ("Pranay:"):
              unitTestCount[1] += parseInt(current[2]);
              break;
            case ("Atsuki:"):
              unitTestCount[2] += parseInt(current[2]);
              break;
            case ("David:"):
              unitTestCount[3] += parseInt(current[2]);
              break;
            case ("Kevin:"):
              unitTestCount[4] += parseInt(current[2]);
              break;
            default:
          }
        }
        this.setState({unitTestCount: unitTestCount});
      });
    });
  }

  componentDidMount() {
    this.getCommitData();
    this.getIssueData();
    this.getTestData();
  }

  render() {
    const names = ["Vishal Gullapalli", "Pranay Nakirekanti", "Atsuki Sawada", "David Wang", "Kevin Wang"];
    const { commitCount, issueCount, unitTestCount } = this.state;
    var tableData = [];
    var totalCommitCount = 0;
    var totalIssueCount = 0;
    var totalUnitTestCount = 0;
    for (var i = 0; i < names.length; i++) {
      tableData.push({
        name: names[i],
        commitCount: commitCount[i],
        issueCount: issueCount[i],
        unitTestCount: unitTestCount[i],
      })
      totalCommitCount += commitCount[i];
      totalIssueCount += issueCount[i];
      totalUnitTestCount += unitTestCount[i];
    }
    return (
      <div className="row">
        <div className="GitLabTableContainer">
          <h4>GitLab Statistics</h4>
          <table className="GitLabTable">
            <tr>
              <th>Name</th>
              <th># of commits</th>
              <th># of issues</th>
              <th># of unit tests</th>
            </tr>
            {tableData.map((entry) => (
              <tr>
                <td>{entry.name}</td>
                <td>{entry.commitCount}</td>
                <td>{entry.issueCount}</td>
                <td>{entry.unitTestCount}</td>
              </tr>
            ))}
            <tr>
              <th>Total</th>
              <th>{totalCommitCount}</th>
              <th>{totalIssueCount}</th>
              <th>{totalUnitTestCount}</th>
            </tr>
          </table>
        </div>
      </div>
    );
  }
}

export default GitLabStatsTable;
