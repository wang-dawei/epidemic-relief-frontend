# Atsuki: 0 tests
# David: 2 tests
# Kevin: 7 tests
# Pranay: 0 tests
# Vishal: 0 tests

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class GUITests(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome('./chromedriver')
    self.outbreaks = 'Outbreaks'
    self.organizations = 'Organizations'
    self.countries = 'Countries'

  # Kevin
  def test1(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me")
    self.assertIn("Epidemic Relief", driver.title)

  # Kevin
  def test2(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me")
    organizationCard = driver.find_element_by_xpath("/html/body/div/div/div/div/div/div/div[2]/div/a")
    self.assertIn(self.organizations, organizationCard.text)
    organizationCard.click()
    header = driver.find_element_by_tag_name("h1").text
    assert header == self.organizations

  # Kevin
  def test3(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me")
    countryCard = driver.find_element_by_xpath("/html/body/div/div/div/div/div/div/div[3]/div/a")
    self.assertIn(self.countries, countryCard.text)
    countryCard.click()
    header = driver.find_element_by_tag_name("h1").text
    assert header == self.countries

  # Kevin
  def test4(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me")
    countryCard = driver.find_element_by_xpath("/html/body/div/div/div/div/div/div/div/div/a")
    self.assertIn(self.outbreaks, countryCard.text)
    countryCard.click()
    header = driver.find_element_by_tag_name("h1").text
    assert header == self.outbreaks

  # Kevin
  def test5(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me/outbreaks/11")
    header = driver.find_element_by_tag_name("h1").text
    assert header == 'Cholera in Yemen'

  # Kevin
  def test6(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me/organizations/7125049")
    header = driver.find_element_by_tag_name("h1").text
    assert header == 'Wistar Institute of Anatomy & Biology'

  # Kevin
  def test7(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me/countries/CHN")
    header = driver.find_element_by_tag_name("h1").text
    assert header == 'China'

  # David
  def test8(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me/about")
    header = driver.find_element_by_tag_name("h1").text
    assert header == 'About Us'

  # David
  def test9(self):
    driver = self.driver
    driver.get("http://epidemicrelief.me")
    searchBox = driver.find_element_by_id("searchBox")
    searchButton = driver.find_element_by_id("searchButton")
    searchBox.send_keys('A')
    searchBox.send_keys('F')
    searchBox.send_keys('R')
    searchBox.send_keys('I')
    searchBox.send_keys('C')
    searchBox.send_keys('A')
    searchBox.send_keys(Keys.RETURN)
    header = driver.find_element_by_tag_name("h1").text
    assert header == 'Search: AFRICA'

  def tearDown(self):
    self.driver.close()

if __name__ == "__main__":
    unittest.main()
